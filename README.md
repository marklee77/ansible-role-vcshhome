marklee77.vcshhome
==================

[![Build Status](https://travis-ci.org/marklee77/ansible-role-vcshhome.svg?branch=master)](https://travis-ci.org/marklee77/ansible-role-vcshhome)

The purpose of this role is to automate installing a user's vcsh config files
into their home directory. The site.yml contains some Ubuntu-specific code to
install vcsh, which could be extended to support other distributions. The role
itself should be distribution-agnostic and not require sudo access.

Example Playbook
-------------------------

    - hosts: all
      roles:
        - marklee77.vcshhome

License
-------

GPLv2

Author Information
------------------

http://marklee77.github.io/
